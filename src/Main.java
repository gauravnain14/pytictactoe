import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Do you want to be X or O?");
        String letter = scanner.next();
        char compLetter = ' ';
        boolean valid = false;
        while(!valid) {
            if (letter.equals("X") || letter.equals("x")) {
                letter = letter.toUpperCase();
                compLetter = 'O';
                valid = true;
            }
            else if (letter.equals("O") || letter.equals("o")) {
                letter = letter.toUpperCase();
                compLetter = 'X';
                valid = true;
            }
            else
                System.out.println("Invalid selection");
        }
        boolean gameOver = false;
        char[][] ticTacToeArray = new char[3][3];
        initializeArr(ticTacToeArray);
        LinkedList<Integer> history = new LinkedList<>();
        System.out.println("Computer plays first");
        int count = 0;
        while(!gameOver) {
            if(count %2 ==0){
                System.out.println("Count: "+count);
                int random = (int) (Math.random() * 9 + 1);
                if(!history.contains(random)) {
                    ticTacToeArray = fillTwoDArray(ticTacToeArray, random, compLetter);
                    printArr(ticTacToeArray);
                    char win = checkIfWin(ticTacToeArray, compLetter);
                    if(win != ' ') {
                        System.out.println(win + " wins!");
                        gameOver = true;
                    }
                    else
                        history.add(random);
                    count++;
                }
                else
                    System.out.println(random + " has already been generated");
                //ticTacToeArray = fillTwoDArray(ticTacToeArray, random, letter.charAt(0));
                }
                else{
                System.out.println("What is your next move? (1-9)");
                int playerLetter = scanner.nextInt();
                ticTacToeArray = fillTwoDArray(ticTacToeArray, playerLetter, letter.charAt(0));
                printArr(ticTacToeArray);
                char win = checkIfWin(ticTacToeArray, letter.charAt(0));
                if(win != ' ') {
                    System.out.println(win + " wins!");
                    gameOver = true;
                }
                else
                    history.add(playerLetter);
                count++;
            }
            }
        }
    public static char[][] fillTwoDArray(char[][] arr, int pos, char letter){
        switch(pos){
            case 1: arr[0][0] = letter; break;
            case 2: arr[0][1] = letter; break;
            case 3: arr[0][2] = letter; break;
            case 4: arr[1][0] = letter; break;
            case 5: arr[1][1] = letter; break;
            case 6: arr[1][2] = letter; break;
            case 7: arr[2][0] = letter; break;
            case 8: arr[2][1] = letter; break;
            case 9: arr[2][2] = letter; break;
            default: break;
    }
    return arr;
    }
    public static void printArr(char[][] arr){
        for(int i = 0; i < arr.length; i++) {
            for(int j = 0; j < arr[0].length; j++){
                System.out.print(" | " + arr[i][j] + " | ");
            }
            System.out.println("\n---------------------");
        }
    }
    public static void initializeArr(char[][] arr){
        for(int i = 0; i < arr.length; i++) {
            for(int j = 0; j < arr[0].length; j++){
                arr[i][j] = ' ';
            }
        }
    }
    public static char checkIfWin(char[][] arr, char player){
        if((arr[0][0] == player) && (arr[0][1] == player) && (arr[0][2] == player) ||
            ((arr[1][0] == player) && (arr[1][1] == player) && (arr[1][2] == player)) ||
            ((arr[2][0] == player) && (arr[2][1] == player) && (arr[2][2] == player)) ||
            ((arr[0][0] == player) && (arr[1][0] == player) && (arr[2][0] == player)) ||
            ((arr[0][1] == player) && (arr[1][1] == player) && (arr[2][1] == player)) ||
            ((arr[0][2] == player) && (arr[1][2] == player) && (arr[2][2] == player)) ||
            ((arr[0][0] == player) && (arr[1][1] == player) && (arr[2][2] == player)) ||
            ((arr[2][0] == player) && (arr[1][1] == player) && (arr[0][2] == player))){
        } else
            player = ' ';
        return player;
    }
}